import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

Color redPrimary = Color.fromRGBO(148, 19, 5, 1);
Color backGround = Color.fromRGBO(116, 125, 144, 1);
Color textColor = Color.fromRGBO(3, 3, 3, 1);
Color bluePrimary = Color.fromRGBO(20, 91, 156, 1);
