// To parse this JSON data, do
//
//     final registroUsuarios = registroUsuariosFromJson(jsonString);

import 'dart:convert';
import 'package:chat/models/usuario.dart';

RegistroUsuarios registroUsuariosFromJson(String str) =>
    RegistroUsuarios.fromJson(json.decode(str));

String registroUsuariosToJson(RegistroUsuarios data) =>
    json.encode(data.toJson());

class RegistroUsuarios {
  RegistroUsuarios({
    this.ok,
    this.usuario,
    this.token,
  });

  bool ok;
  Usuario usuario;
  String token;

  factory RegistroUsuarios.fromJson(Map<String, dynamic> json) =>
      RegistroUsuarios(
        ok: json["ok"],
        usuario: Usuario.fromJson(json["usuario"]),
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "ok": ok,
        "usuario": usuario.toJson(),
        "token": token,
      };
}
