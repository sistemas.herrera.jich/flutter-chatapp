import 'dart:typed_data';

import 'package:chat/helpers/colored.dart';
import 'package:chat/helpers/mostrarAlertas.dart';
import 'package:chat/models/data.dart';
import 'package:chat/models/vehiculos.dart';
import 'package:chat/pages/ordenInvoice/pdf_api.dart';
import 'package:chat/pages/ordenInvoice/pdf_salida.dart';
import 'package:chat/pages/ordenInvoice/signatura_final.dart';
import 'package:chat/services/auth_services.dart';
import 'package:chat/services/socket_service.dart';
import 'package:chat/services/vehiculos_services.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:open_file/open_file.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HistorialVehiculo extends StatefulWidget {
  HistorialVehiculo({Key key}) : super(key: key);

  @override
  _HistorialVehiculoState createState() => _HistorialVehiculoState();
}

class _HistorialVehiculoState extends State<HistorialVehiculo> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  final vehiculosServices = new GetVehiculos();
  final bool conection = false;
  List<Vehiculo> vehiculos = [];

  @override
  void initState() {
    super.initState();
    _cargarUsuarios();
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    final socketService = Provider.of<SocketServices>(context);
    //final status = socketService.serverStatus;
    final usuario = authService.usuario;

    return Scaffold(
      backgroundColor: Color.fromRGBO(116, 125, 144, 1),
      appBar: appBarView(usuario, socketService, context),
      body: SmartRefresher(
        controller: _refreshController,
        enablePullDown: true,
        child: _listViewUsuarios(),
        header: WaterDropHeader(
          complete: Icon(
            Icons.check,
            color: Colors.blue[400],
          ),
          waterDropColor: Colors.blue[400],
        ),
        onRefresh: _cargarUsuarios,
      ),
      floatingActionButton: FloatingActionButton(
        elevation: 10,
        backgroundColor: redPrimary,
        onPressed: () => {Navigator.pushNamed(context, 'orden-entrada')},
        tooltip: 'Agregar nueva Orden',
        child: Icon(Icons.add, color: textColor, size: 50),
      ),
    );
  }

  AppBar appBarView(
      usuario, SocketServices socketService, BuildContext context) {
    return AppBar(
      title: Text(usuario.nombre.substring(0, 15),
          style: TextStyle(color: Color.fromRGBO(3, 3, 3, 1))),
      elevation: 1,
      backgroundColor: Color.fromRGBO(148, 19, 5, 1),
      leading: IconButton(
        icon: Icon(
          Icons.exit_to_app,
          color: Color.fromRGBO(3, 3, 3, 1),
        ),
        onPressed: () {
          socketService.disconnect();
          AuthService.deleteToken();
          Navigator.pushReplacementNamed(context, 'login');
        },
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.person_add),
          onPressed: () => Navigator.pushNamed(context, 'register'),
        ),
      ],
    );
  }

  ListView _listViewUsuarios() {
    return ListView.separated(
        physics: BouncingScrollPhysics(),
        itemBuilder: (_, i) => _usuarioListTitle(vehiculos[i]),
        separatorBuilder: (_, i) => Divider(),
        itemCount: vehiculos.length);
  }

  ExpansionTile _usuarioListTitle(Vehiculo vehiculo) {
    return ExpansionTile(
      title: Text('${vehiculo.nombre} ${vehiculo.rfc}'),
      subtitle: Text(vehiculo.fechaIngreso.toString()),
      leading: CircleAvatar(
          child: Text(vehiculo.nombre.substring(0, 2),
              style: TextStyle(color: Colors.red[600])),
          backgroundColor: Colors.black87),
      trailing: Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
            color: vehiculo.etapa == 6 ? Colors.green : Colors.redAccent,
            borderRadius: BorderRadius.circular(100)),
      ),
      children: <Widget>[
        ListTile(
          title: Container(
            alignment: Alignment.center,
            color: redPrimary,
            padding: EdgeInsets.all(10),
            child: Text("Firmar orden de salida",
                style: TextStyle(
                    color: textColor,
                    letterSpacing: 2,
                    fontWeight: FontWeight.bold)),
          ),
          onTap: () => {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SignatureFinal(Data(
                      uidVehiculo: vehiculo.uid,
                      uidPdfEntradas: vehiculo.pdf.id))),
            )
          },
        ),
        ListTile(
          title: Container(
            alignment: Alignment.center,
            color: redPrimary,
            padding: EdgeInsets.all(10),
            child: Text("Ver PDF Orden entrada",
                style: TextStyle(
                    color: textColor,
                    letterSpacing: 2,
                    fontWeight: FontWeight.bold)),
          ),
          onTap: () => onSubmit(vehiculo.uid, vehiculo.pdf.urlentrada),
        ),
        ListTile(
          title: Container(
            alignment: Alignment.center,
            color: redPrimary,
            padding: EdgeInsets.all(10),
            child: Text("Ver PDF orden de salida",
                style: TextStyle(
                    color: textColor,
                    letterSpacing: 2,
                    fontWeight: FontWeight.bold)),
          ),
          onTap: () => {
            if (vehiculo.pdf.urlsalida == '')
              {
                mostrarAlertas(
                    context, "Error", 'No se ha firmado una orden de salida')
              }
            else
              {onSubmitSalida(vehiculo.uid, vehiculo.pdf)}
          },
        ),
      ],
    );
  }

  Future onSubmit(uid, url) async {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => Center(child: CircularProgressIndicator()),
    );

    List<Vehiculo> vehiculos = await vehiculosServices.getVehiculos(uid);

    ByteData imageSignatureCliente =
        await NetworkAssetBundle(Uri.parse(url)).load(url);
    ByteData logo = await rootBundle.load('assets/images/logo_pdf.jpeg');
    ByteData diagrama = await rootBundle.load('assets/images/diagrama.png');

    final file = await PdfApi.generatePDF(
        logo: logo,
        imageSignatureCliente: imageSignatureCliente,
        vehiculo: vehiculos,
        diagrama: diagrama);
    Navigator.of(context).pop();
    await OpenFile.open(file.path);
  }

  _cargarUsuarios() async {
    this.vehiculos = await vehiculosServices.vehiculosLista();
    if (!mounted) return;
    setState(() {});
    _refreshController.refreshCompleted();
  }

  Future onSubmitSalida(String uid, PDF pdf) async {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => Center(child: CircularProgressIndicator()),
    );

    List<Vehiculo> vehiculos = await vehiculosServices.getVehiculos(uid);

    ByteData imageSignatureCliente =
        await NetworkAssetBundle(Uri.parse(pdf.urlsalida)).load(pdf.urlsalida);
    ByteData logo = await rootBundle.load('assets/images/logo_pdf.jpeg');
    final file = await PdfSalida.generatePDF(
        logo: logo,
        imageSignatureCliente: imageSignatureCliente,
        vehiculo: vehiculos,
        pdf: pdf);

    Navigator.of(context).pop();
    await OpenFile.open(file.path);
  }
}
