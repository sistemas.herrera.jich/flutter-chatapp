import 'dart:io';
import 'dart:typed_data';

import 'package:chat/models/vehiculos.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class PdfSalida {
  static Future<File> generatePDF(
      {ByteData logo,
      ByteData imageSignatureCliente,
      List<Vehiculo> vehiculo,
      PDF pdf}) async {
    final document = PdfDocument();
    final page = document.pages.add();

    drawHeader(page, logo, vehiculo);
    drawSignatureCliente(page, imageSignatureCliente);
    drawListServicios(page, pdf);
    drawPen(page, 456);
    drawPen(page, 470);
    drawFooter(page, pdf, vehiculo);
    drawPen(page, 380);
    drawPen(page, 366);
    drawPen(page, 120);

    return saveFile(document);
  }

  static Future<File> saveFile(PdfDocument document) async {
    final path = await getApplicationDocumentsDirectory();
    final fileName =
        path.path + '/Orden${DateTime.now().toIso8601String()}.pdf';
    final file = File(fileName);
    file.writeAsBytes(document.save());
    document.dispose();
    return file;
  }

  static PdfLayoutResult drawHeader(
      PdfPage page, ByteData logo, List<Vehiculo> vehiculo) {
    final pageSize = page.getClientSize();
    final PdfBitmap image = PdfBitmap(logo.buffer.asUint8List());

    //String numOrden = vehiculo[0].uid;
    String lugar = vehiculo[0].usuario.sucursal.ciudad +
        " " +
        vehiculo[0].usuario.sucursal.estado +
        " ";
    String fechaEntrada = DateTime.parse(DateTime.now().toString()).toString();
    String nombre = vehiculo[0].nombre;
    //String rfc = vehiculo[0].rfc;
    String modelo = vehiculo[0].modelo;
    String marca = vehiculo[0].marca;
    String nseries = vehiculo[0].nseries;
    //String nmotor = vehiculo[0].nmotor;
    String placa = vehiculo[0].placa;
    //String tipo = vehiculo[0].tipo;
    String color = vehiculo[0].color;
    //String telefono = vehiculo[0].telefono;
    String kmrecorridos = vehiculo[0].kmrecorridos;

    page.graphics.drawImage(
      image,
      Rect.fromLTWH(0, 0, pageSize.width - 350, 90),
    );

    final encabezado =
        'REPARACIÓN Y/O MANTENIMIENTO DE VEHÍCULOS ORDEN DE SALIDA';
    page.graphics.drawString(
      encabezado,
      PdfStandardFont(PdfFontFamily.helvetica, 12),
      bounds: Rect.fromLTWH(200, 0, 300, 50),
      brush: PdfBrushes.black,
      format: PdfStringFormat(
        alignment: PdfTextAlignment.center,
        lineAlignment: PdfVerticalAlignment.middle,
      ),
    );

    final contentFont = PdfStandardFont(PdfFontFamily.helvetica, 9);

    page.graphics.drawString(
      'Fecha: $lugar $fechaEntrada',
      contentFont,
      brush: PdfBrushes.black,
      bounds: Rect.fromLTWH(200, 40, 300, 33),
      format: PdfStringFormat(
        alignment: PdfTextAlignment.left,
        lineAlignment: PdfVerticalAlignment.bottom,
      ),
    );

    page.graphics.drawString(
      'JHK MULTISERVICIOS SA DE CV DOMICILIO: BLVD. FRANCISCO I. MADERO #2151 COL. MIGUEL HIDALGO CULIACAN, SINALOA R.F.C.: JMU-15031-LX7',
      contentFont,
      brush: PdfBrushes.black,
      bounds: Rect.fromLTWH(0, 75, 200, 55),
      format: PdfStringFormat(
        alignment: PdfTextAlignment.left,
        lineAlignment: PdfVerticalAlignment.bottom,
      ),
    );

    final contentFontDatosVehiculo =
        PdfStandardFont(PdfFontFamily.helvetica, 12);

    final text = '''NOMBRE: $nombre ''';
    PdfTextElement(text: text, font: contentFontDatosVehiculo).draw(
        page: page,
        bounds: Rect.fromLTWH(0, 150, page.getClientSize().width, 30));

    final dataGridInfo = PdfGrid();
    dataGridInfo.columns.add(count: 3);
    final headerRowVehiculo = dataGridInfo.headers.add(1)[0];
    headerRowVehiculo.style.backgroundBrush =
        PdfSolidBrush(PdfColor(165, 165, 165));
    headerRowVehiculo.style.textBrush = PdfBrushes.white;
    headerRowVehiculo.cells[0].value = 'MARCA';
    headerRowVehiculo.cells[1].value = 'MODELO';
    headerRowVehiculo.cells[2].value = 'COLOR';
    headerRowVehiculo.style.font =
        PdfStandardFont(PdfFontFamily.helvetica, 9, style: PdfFontStyle.bold);
    final row = dataGridInfo.rows.add();

    row.cells[0].value = marca;
    row.cells[1].value = modelo;
    row.cells[2].value = color;

    dataGridInfo.applyBuiltInStyle(PdfGridBuiltInStyle.gridTable1Light);

    for (int i = 0; i < headerRowVehiculo.cells.count; i++) {
      headerRowVehiculo.cells[i].style.cellPadding =
          PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
    }

    for (int i = 0; i < dataGridInfo.rows.count; i++) {
      final row = dataGridInfo.rows[i];
      for (int j = 0; j < row.cells.count; j++) {
        final cell = row.cells[j];

        cell.style.cellPadding =
            PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
      }
    }

    dataGridInfo.draw(
      page: page,
      bounds: Rect.fromLTWH(0, 180, 0, 0),
    );

    final dataGridInfo2 = PdfGrid();
    dataGridInfo2.columns.add(count: 3);
    final headerRow = dataGridInfo2.headers.add(1)[0];
    headerRow.style.backgroundBrush = PdfSolidBrush(PdfColor(165, 165, 165));
    headerRow.style.textBrush = PdfBrushes.white;
    headerRow.cells[0].value = 'PLACAS';
    headerRow.cells[1].value = 'N° SERIE';
    headerRow.cells[2].value = 'KM RECORRIDOS';
    headerRow.style.font =
        PdfStandardFont(PdfFontFamily.helvetica, 9, style: PdfFontStyle.bold);
    final row2 = dataGridInfo2.rows.add();

    row2.cells[0].value = placa;
    row2.cells[1].value = nseries;
    row2.cells[2].value = kmrecorridos;

    dataGridInfo2.applyBuiltInStyle(PdfGridBuiltInStyle.gridTable1Light);

    for (int i = 0; i < headerRow.cells.count; i++) {
      headerRow.cells[i].style.cellPadding =
          PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
    }

    for (int i = 0; i < dataGridInfo2.rows.count; i++) {
      final row = dataGridInfo2.rows[i];
      for (int j = 0; j < row.cells.count; j++) {
        final cell = row.cells[j];

        cell.style.cellPadding =
            PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
      }
    }

    dataGridInfo2.draw(
      page: page,
      bounds: Rect.fromLTWH(0, 230, 0, 0),
    );

    return PdfTextElement(
      format: PdfStringFormat(lineSpacing: 0),
    ).draw(
      page: page,
      bounds: Rect.fromLTWH(
        10,
        400,
        pageSize.width / 3,
        pageSize.height - 100,
      ),
    );
  }

  static void drawListServicios(PdfPage page, PDF pdf) {
    final pageSize = page.getClientSize();

    String servicio = pdf.servicio;

    page.graphics.drawString(
        'SERVICIOS REALIZADOS', PdfStandardFont(PdfFontFamily.helvetica, 12),
        bounds: Rect.fromLTWH(
            pageSize.width / 3 + 10, pageSize.height - 470, 150, 40));

    page.graphics.drawString(
        servicio, PdfStandardFont(PdfFontFamily.helvetica, 9),
        bounds: Rect.fromLTWH(
            10, pageSize.height - 450, pageSize.width - 100, 100));
  }

  static void drawFooter(PdfPage page, PDF pdf, List<Vehiculo> vehiculo) {
    final pageSize = page.getClientSize();
    final String observacion = pdf.observacion;

    page.graphics.drawString(
        'OBSERVACIONES', PdfStandardFont(PdfFontFamily.helvetica, 12),
        bounds: Rect.fromLTWH(
            pageSize.width / 3 + 10, pageSize.height - 380, 150, 40));

    page.graphics.drawString(
        observacion, PdfStandardFont(PdfFontFamily.helvetica, 9),
        bounds: Rect.fromLTWH(
            10, pageSize.height - 340, pageSize.width - 100, 100));
    page.graphics.drawString(
        'EL CONSUMIDOR ACEPTA RECIBO DE ENTREGA DE LA UNIDAD CORRESPONDIENTE',
        PdfStandardFont(PdfFontFamily.helvetica, 9),
        bounds: Rect.fromLTWH(
            10, pageSize.height - 60, pageSize.width / 2 - 30, 40));
    PdfTextElement(
      format: PdfStringFormat(lineSpacing: 0),
    ).draw(
      page: page,
      bounds: Rect.fromLTWH(10, 350, pageSize.width / 3, pageSize.height - 100),
    );

    page.graphics.drawString(
        vehiculo[0].usuario.name + '\n' + vehiculo[0].usuario.sucursal.nombre,
        PdfStandardFont(PdfFontFamily.helvetica, 9),
        bounds: Rect.fromLTWH(pageSize.width / 2, pageSize.height - 100,
            pageSize.width / 2, pageSize.height - 100));

    page.graphics.drawString(
        'EL USUARIO ACEPTA QUE SE REALIZO EL TRABAJO MENCIONADO DE LA UNIDAD CORRESPONDIENTE',
        PdfStandardFont(PdfFontFamily.helvetica, 9),
        bounds: Rect.fromLTWH(
            pageSize.width / 2, pageSize.height - 60, pageSize.width / 2, 40));
  }

  static void drawSignature(PdfPage page, ByteData imageSignature) {
    final pageSize = page.getClientSize();
    final PdfBitmap image = PdfBitmap(imageSignature.buffer.asUint8List());

    page.graphics.drawImage(
      image,
      Rect.fromLTWH(400, pageSize.height - 100, 100, 40),
    );
  }

  static void drawSignatureCliente(PdfPage page, ByteData imageSignature) {
    final pageSize = page.getClientSize();
    final PdfBitmap image = PdfBitmap(imageSignature.buffer.asUint8List());

    page.graphics.drawImage(
      image,
      Rect.fromLTWH(20, pageSize.height - 100, 100, 40),
    );
  }

  static void drawPen(PdfPage page, double y) {
    final pageSize = page.getClientSize();
    final color = PdfColor(165, 165, 165);
    final linePen = PdfPen(color, dashStyle: PdfDashStyle.solid);
    final yLine = pageSize.height - y;

    page.graphics.drawLine(
      linePen,
      Offset(0, yLine),
      Offset(pageSize.width, yLine),
    );
  }

  static void drawPenX(PdfPage page, double y) {
    final pageSize = page.getClientSize();
    final color = PdfColor(165, 165, 165);
    final linePen = PdfPen(color, dashStyle: PdfDashStyle.solid);
    final yLine = pageSize.height - y;

    page.graphics.drawLine(
      linePen,
      Offset(0, yLine),
      Offset(pageSize.width, yLine),
    );
  }
}
