import 'package:chat/helpers/colored.dart';
import 'package:chat/helpers/mostrarAlertas.dart';
import 'package:chat/models/vehiculo.dart';
import 'package:chat/services/auth_services.dart';
import 'package:chat/services/socket_service.dart';
import 'package:chat/widgets/boton_azul.dart';
import 'package:chat/widgets/custom_inputs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OrdenServicio extends StatelessWidget {
  const OrdenServicio({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGround,
      appBar: new AppBar(
        backgroundColor: redPrimary,
        centerTitle: true,
        title: new Text(
          'DATOS FINALES',
          style: TextStyle(color: textColor),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            child: OrdenServicioForm(),
          ),
        ),
      ),
    );
  }
}

class OrdenServicioForm extends StatefulWidget {
  const OrdenServicioForm({Key key}) : super(key: key);

  @override
  _OrdenServicioFormState createState() => _OrdenServicioFormState();
}

class _OrdenServicioFormState extends State<OrdenServicioForm> {
  TextEditingController trabajoRealizar = new TextEditingController();
  TextEditingController diagnostico = new TextEditingController();
  TextEditingController riesgo = new TextEditingController();

  SocketServices socketService;
  AuthService auhtService;

  @override
  void initState() {
    super.initState();
    this.socketService = Provider.of<SocketServices>(context, listen: false);
    this.auhtService = Provider.of<AuthService>(context, listen: false);

    socketService.connect();
  }

  @override
  Widget build(BuildContext context) {
    VehiculoUID args = ModalRoute.of(context).settings.arguments;

    return Container(
      margin: EdgeInsets.only(top: 50, left: 20, right: 20),
      child: Column(children: <Widget>[
        CustomInput(
            controller: trabajoRealizar,
            boolean: false,
            hintText:
                'Operaciones a efectuar y elementos a reparar o sustituir',
            icon: Icons.medication,
            inputType: TextInputType.text),
        CustomInput(
            controller: diagnostico,
            boolean: false,
            hintText: 'Diagnostico',
            icon: Icons.medical_services,
            inputType: TextInputType.text),
        CustomInput(
            controller: riesgo,
            boolean: false,
            hintText:
                'Riesgo del vehiculo derivados de la realización del diagnostico',
            icon: Icons.dangerous,
            inputType: TextInputType.text),
        SizedBox(height: 60),
        Container(
          width: MediaQuery.of(context).size.width / 1.5,
          height: MediaQuery.of(context).size.height / 15,
          child: BotonAzul(
            text: "Continuar",
            onPressed: () {
              FocusScope.of(context).unfocus();
              if (trabajoRealizar.text != "" &&
                  diagnostico.text != "" &&
                  riesgo.text != "") {
                var object = {
                  'uid': '',
                  'uidVehiculo': args.uid,
                  'trabajoRealizar': trabajoRealizar.text,
                  'diagnostico': diagnostico.text,
                  'riesgos': riesgo.text
                };

                this.socketService.socket.emit('orden-servicio', object);

                Navigator.pushNamed(context, 'signature',
                    arguments: VehiculoUID(args.uid));
              } else {
                mostrarAlertas(context, 'Registro Incorrecto',
                    'Llene todos los campos para continuar');
              }
            },
          ),
        )
      ]),
    );
  }
}
