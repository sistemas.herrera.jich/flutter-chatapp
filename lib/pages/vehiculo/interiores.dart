import 'package:chat/helpers/colored.dart';
import 'package:chat/models/vehiculo.dart';
import 'package:chat/services/auth_services.dart';
import 'package:chat/services/socket_service.dart';
import 'package:chat/widgets/boton_azul.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Interiores extends StatefulWidget {
  @override
  InterioresState createState() => new InterioresState();
}

class InterioresState extends State<Interiores> {
  List<CheckBoxListTileModel> checkBoxListTileModel =
      CheckBoxListTileModel.getUsers();
  SocketServices socketService;
  AuthService auhtService;

  @override
  void initState() {
    super.initState();
    this.socketService = Provider.of<SocketServices>(context, listen: false);
    this.auhtService = Provider.of<AuthService>(context, listen: false);

    socketService.connect();
  }

  @override
  Widget build(BuildContext context) {
    VehiculoUID args = ModalRoute.of(context).settings.arguments;

    return new Scaffold(
      backgroundColor: backGround,
      appBar: new AppBar(
        backgroundColor: redPrimary,
        centerTitle: true,
        title: new Text(
          'INTERIORES',
          style: TextStyle(color: textColor),
        ),
      ),
      body: Card(
        color: backGround,
        child: new Container(
          padding: new EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[0].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[0].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[0].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 0);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[1].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[1].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[1].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 1);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[2].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[2].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[2].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 2);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[3].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[3].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[3].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 3);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[4].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[4].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[4].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 4);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[5].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[5].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[5].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 5);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[6].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[6].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[6].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 6);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[7].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[7].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[7].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 7);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[8].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[8].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[8].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 8);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[9].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[9].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[9].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 9);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[10].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[10].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[10].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 10);
                  }),
              CheckboxListTile(
                  activeColor: Colors.pink[300],
                  dense: true,
                  //font change
                  title: new Text(
                    checkBoxListTileModel[11].title,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0.5),
                  ),
                  value: checkBoxListTileModel[11].isCheck,
                  secondary: Container(
                    height: 50,
                    width: 50,
                    child: checkBoxListTileModel[11].img,
                  ),
                  onChanged: (bool val) {
                    itemChange(val, 11);
                  }),
              Container(
                width: MediaQuery.of(context).size.width / 2,
                child: BotonAzul(
                  text: "Continuar",
                  onPressed: () {
                    FocusScope.of(context).unfocus();
                    if (true) {
                      var objects = {
                        'uid': '',
                        'uidVehiculo': args.uid, //ID Vehiculo ralacion
                        'itablero': checkBoxListTileModel[0].isCheck,
                        'calefaccion': checkBoxListTileModel[1].isCheck,
                        'radio': checkBoxListTileModel[2].isCheck,
                        'bocinas': checkBoxListTileModel[3].isCheck,
                        'encendedor': checkBoxListTileModel[4].isCheck,
                        'espejoRetrovisor': checkBoxListTileModel[5].isCheck,
                        'ceniceros': checkBoxListTileModel[6].isCheck,
                        'cinturones': checkBoxListTileModel[7].isCheck,
                        'botonesInteriores': checkBoxListTileModel[8].isCheck,
                        'manijasInteriores': checkBoxListTileModel[9].isCheck,
                        'tapetes': checkBoxListTileModel[10].isCheck,
                        'vestiduras': checkBoxListTileModel[11].isCheck
                      };

                      this
                          .socketService
                          .socket
                          .emit('interiores-entrada', objects);
                      Navigator.pushNamed(context, 'accesorios',
                          arguments: VehiculoUID(args.uid));
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void itemChange(bool val, int index) {
    setState(() {
      checkBoxListTileModel[index].isCheck = val;
    });
  }
}

class CheckBoxListTileModel {
  int userId;
  Icon img;
  String title;
  bool isCheck;

  CheckBoxListTileModel({this.userId, this.img, this.title, this.isCheck});

  static List<CheckBoxListTileModel> getUsers() {
    return <CheckBoxListTileModel>[
      CheckBoxListTileModel(
          userId: 1,
          img: Icon(Icons.lightbulb),
          title: "Instrumentos de tablero",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 2,
          img: Icon(Icons.lightbulb_outline),
          title: "Calefacción",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 3,
          img: Icon(Icons.lightbulb_outline),
          title: "Radio",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 4,
          img: Icon(Icons.signal_cellular_alt_outlined),
          title: "Bocinas",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 5,
          img: Icon(Icons.add_to_photos),
          title: "Encendendor",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 6,
          img: Icon(Icons.ac_unit_sharp),
          title: "Espejo Retrovisor",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 7,
          img: Icon(Icons.device_hub),
          title: "Ceniceros",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 8,
          img: Icon(Icons.settings_outlined),
          title: "Cinturones",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 9,
          img: Icon(Icons.radio_button_off),
          title: "Botones de Interiores",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 10,
          img: Icon(Icons.circle_notifications),
          title: "Manijas de Interiores",
          isCheck: false),
      CheckBoxListTileModel(
          userId: 11, img: Icon(Icons.circle), title: "Tapete", isCheck: false),
      CheckBoxListTileModel(
          userId: 12,
          img: Icon(Icons.circle_rounded),
          title: "Vestiduras",
          isCheck: false),
    ];
  }
}
