import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import 'package:chat/services/auth_services.dart';
import 'package:chat/services/chat_services.dart';
import 'package:chat/services/socket_service.dart';
import 'package:chat/widgets/chatMessage.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> with TickerProviderStateMixin {
  final _textController = new TextEditingController();
  final _focusNode = new FocusNode();
  ChatService chatService;
  SocketServices socketService;
  AuthService auhtService;
  bool _escribiendo = false;
  List<ChatMessage> message = [];

  @override
  void initState() {
    super.initState();
    this.chatService = Provider.of<ChatService>(context, listen: false);
    this.socketService = Provider.of<SocketServices>(context, listen: false);
    this.auhtService = Provider.of<AuthService>(context, listen: false);

    this.socketService.socket.on('private-message', _escuchandoMsg);
  }

  void _escuchandoMsg(dynamic data) {
    ChatMessage msg = new ChatMessage(
        texto: data['message'],
        uid: data['de'],
        animationController: AnimationController(
            vsync: this, duration: Duration(milliseconds: 300)));

    setState(() {
      message.insert(0, msg);
    });

    //Arrancar la animación del mensaje
    msg.animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    final inOrderTo = chatService.inOrderTo;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 1,
        title: Column(
          children: <Widget>[
            CircleAvatar(
              child: Text(
                inOrderTo.nombre.substring(0, 2),
                style: TextStyle(fontSize: 12),
              ),
              backgroundColor: Colors.blue[100],
              maxRadius: 14,
            ),
            SizedBox(height: 3),
            Text(
              inOrderTo.nombre,
              style: TextStyle(color: Colors.blue, fontSize: 10),
            )
          ],
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Flexible(
                child: ListView.builder(
                    reverse: true,
                    itemCount: message.length,
                    itemBuilder: (_, i) => message[i])),
            Divider(height: 1),
            Container(
              height: 100,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: _inputBoxChat(),
            )
          ],
        ),
      ),
    );
  }

  Widget _inputBoxChat() {
    return SafeArea(
        child: Container(
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        children: <Widget>[
          Flexible(
              child: TextField(
            controller: _textController,
            onSubmitted: _handleSubmit,
            onChanged: (String texto) {
              setState(() {
                if (texto.trim().length > 0) {
                  _escribiendo = true;
                } else {
                  _escribiendo = false;
                }
              });
            },
            decoration: InputDecoration(hintText: 'Escribir mensaje'),
            focusNode: _focusNode,
          )),
          // Boton de enviar
          Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: Platform.isIOS
                ? CupertinoButton(
                    child: Text("Enviar"),
                    onPressed: _escribiendo
                        ? () {
                            _handleSubmit(_textController.text.trim());
                          }
                        : null)
                : Container(
                    margin: EdgeInsets.symmetric(horizontal: 4.0),
                    child: IconTheme(
                      data: IconThemeData(color: Colors.blue[400]),
                      child: IconButton(
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          icon: Icon(Icons.send_outlined),
                          onPressed: _escribiendo
                              ? () {
                                  _handleSubmit(_textController.text.trim());
                                }
                              : null),
                    )),
          )
        ],
      ),
    ));
  }

  _handleSubmit(String texto) {
    if (texto.trim().length == 0) {
      return;
    }
    final newMessage = new ChatMessage(
      uid: '123',
      texto: texto,
      animationController: AnimationController(
          vsync: this, duration: Duration(milliseconds: 400)),
    );

    message.insert(0, newMessage);
    newMessage.animationController.forward();
    _focusNode.requestFocus();
    _textController.clear();

    setState(() {
      _escribiendo = false;
    });
    this.socketService.socket.emit('private-message', {
      'de': this.auhtService.usuario.uid,
      'para': this.chatService.inOrderTo.uid,
      'message': texto
    });
  }

  @override
  void dispose() {
    for (ChatMessage message in message) {
      message.animationController.dispose();
    }

    //Desconectarme del call back socketService
    this.socketService.socket.off('private-message');

    super.dispose();
  }
}
