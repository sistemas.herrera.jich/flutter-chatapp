import 'package:flutter/material.dart';

class CustomInput extends StatelessWidget {
  final TextInputType inputType;
  final TextEditingController controller;
  final IconData icon;
  final bool boolean;
  final String hintText;

  const CustomInput(
      {Key key,
      this.inputType = TextInputType.text,
      @required this.controller,
      @required this.icon,
      @required this.boolean,
      this.hintText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.only(top: 5, left: 5, bottom: 5, right: 15),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black.withOpacity(0.05),
                offset: Offset(0, 5),
                blurRadius: 5)
          ]),
      child: TextField(
        obscureText: this.boolean,
        autocorrect: false,
        keyboardType: this.inputType,
        controller: this.controller,
        decoration: InputDecoration(
            prefixIcon: Icon(icon),
            focusedBorder: InputBorder.none,
            border: InputBorder.none,
            hintText: this.hintText),
      ),
    );
  }
}
