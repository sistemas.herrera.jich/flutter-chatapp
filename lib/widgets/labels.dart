import 'package:flutter/material.dart';

class Labels extends StatelessWidget {
  final String route;
  final String textRoute;
  final String textEncabezado;

  const Labels(
      {Key key,
      @required this.route,
      @required this.textRoute,
      @required this.textEncabezado})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Text(
            this.textEncabezado,
            style: TextStyle(
                color: Colors.black54,
                fontSize: 15,
                fontWeight: FontWeight.w300),
          ),
          SizedBox(
            height: 10,
          ),
          GestureDetector(
            child: Text(
              this.textRoute,
              style: TextStyle(
                  color: Colors.blue[600],
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
            onTap: () => {Navigator.pushNamed(context, this.route)},
          )
        ],
      ),
    );
  }
}
