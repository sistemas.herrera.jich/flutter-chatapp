import 'package:flutter/material.dart';

class BotonAzul extends StatelessWidget {
  final String text;
  final Function onPressed;

  const BotonAzul({Key key, @required this.text, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 2,
          primary: Color.fromRGBO(148, 19, 5, 1),
          onPrimary: Color.fromRGBO(3, 3, 3, 1),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        ),
        child: Container(
          width: double.infinity,
          child: Center(
            child: Text(
              this.text,
              style: TextStyle(color: Color.fromRGBO(3, 3, 3, 1), fontSize: 17),
            ),
          ),
        ),
        onPressed: this.onPressed);
  }
}
