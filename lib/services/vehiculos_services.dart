import 'package:http/http.dart' as http;

import 'package:chat/models/vehiculos.dart';
import 'package:chat/global/enviroment.dart';
import 'package:chat/services/auth_services.dart';

class GetVehiculos {
  Future<List<Vehiculo>> getVehiculos(uid) async {
    try {
      final url = Uri.parse('${Enviroments.apiUrl}/vehiculos/$uid');
      Map<String, String> headers = {
        "Content-type": "application/json",
        "x-token": await AuthService.getToken()
      };
      final resp = await http.get(url, headers: headers);

      final data = vehiculosFromJson(resp.body);
      return data.vehiculo;
    } catch (e) {
      return [];
    }
  }

  Future<List<Vehiculo>> vehiculosLista() async {
    try {
      final url = Uri.parse('${Enviroments.apiUrl}/vehiculos');
      Map<String, String> headers = {
        "Content-type": "application/json",
        "x-token": await AuthService.getToken()
      };
      final resp = await http.get(url, headers: headers);
      final data = vehiculosFromJson(resp.body);
      return data.vehiculo;
    } catch (e) {
      return [];
    }
  }
}
