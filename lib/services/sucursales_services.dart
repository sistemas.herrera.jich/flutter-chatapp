import 'package:http/http.dart' as http;

import 'package:chat/models/sucursales.dart';
import 'package:chat/global/enviroment.dart';
import 'package:chat/services/auth_services.dart';

class SucursalServices {
  Future<List<Sucursal>> getSucursal() async {
    try {
      final url = Uri.parse('${Enviroments.apiUrl}/sucursal');
      Map<String, String> headers = {
        "Content-type": "application/json",
        "x-token": await AuthService.getToken()
      };
      final resp = await http.get(url, headers: headers);

      final sucursales = sucursalesFromJson(resp.body);
      print(sucursales.sucursal);
      return sucursales.sucursal;
    } catch (e) {
      print(e);
      return [];
    }
  }
}
