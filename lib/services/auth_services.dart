import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:chat/global/enviroment.dart';
import 'package:chat/models/loginResponce.dart';
import 'package:chat/models/usuario.dart';

class AuthService with ChangeNotifier {
  Usuario usuario;
  bool _authenticando = false;

  final _storage = new FlutterSecureStorage();

  bool get autenticando => this._authenticando;

  set autenticando(bool valor) {
    this._authenticando = valor;
    notifyListeners();
  }

  //Getters y Setters
  static Future<String> getToken() async {
    final _storage = new FlutterSecureStorage();
    final token = await _storage.read(key: 'token');
    return token;
  }

  static Future<String> deleteToken() async {
    final _storage = new FlutterSecureStorage();
    await _storage.delete(key: 'token');
    return "borrrado";
  }

  Future<bool> login(String email, String password) async {
    this.autenticando = true;

    final data = {'email': email, 'password': password};
    final url = Uri.parse('${Enviroments.apiUrl}/login');
    Map<String, String> headers = {"Content-type": "application/json"};
    final resp = await http.post(url, headers: headers, body: jsonEncode(data));

    this.autenticando = false;
    if (resp.statusCode == 200) {
      final loginResponce = loginResponceFromJson(resp.body);
      this.usuario = loginResponce.usuario;
      await this._guardarToken(loginResponce.token);
      return true;
    } else {
      return false;
    }
  }

  Future register(
      String nombre, String email, String password, String sucursal) async {
    this.autenticando = true;

    final data = {
      'name': nombre,
      'email': email,
      'password': password,
      'sucursal': sucursal
    };
    final url = Uri.parse('${Enviroments.apiUrl}/login/create');
    Map<String, String> headers = {"Content-type": "application/json"};
    final resp = await http.post(url, headers: headers, body: jsonEncode(data));

    this.autenticando = false;
    if (resp.statusCode == 200) {
      //final registroResponce = registroUsuariosFromJson(resp.body);
      //this.usuario = registroResponce.usuario;
      //await this._guardarToken(registroResponce.token);
      return true;
    } else {
      final respBody = jsonDecode(resp.body);
      return respBody['msg'];
    }
  }

  Future _guardarToken(String token) async {
    return await _storage.write(key: 'token', value: token);
  }

  Future _logOut() async {
    return await _storage.delete(key: 'token');
  }

  Future<bool> isLoggedIn() async {
    final token = await this._storage.read(key: 'token');

    final url = Uri.parse('${Enviroments.apiUrl}/login/renew');
    Map<String, String> headers = {
      "Content-type": "application/json",
      "x-token": token
    };

    final resp = await http.get(url, headers: headers);

    if (resp.statusCode == 200) {
      final loginResponce = loginResponceFromJson(resp.body);
      this.usuario = loginResponce.usuario;
      await this._guardarToken(loginResponce.token);
      return true;
    } else {
      this._logOut();
      return false;
    }
  }
}
