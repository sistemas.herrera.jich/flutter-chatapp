import 'dart:io';

class Enviroments {
  static String apiUrl = Platform.isAndroid
      ? 'https://socket-serverah.herokuapp.com/api'
      : 'https://socket-serverah.herokuapp.com/api';

  static String socketUrl = Platform.isAndroid
      ? 'https://socket-serverah.herokuapp.com'
      : 'https://socket-serverah.herokuapp.com';
}
